% Octave script to compute magnetic field lines (streamlines)
% from a vector file generated using the Lua script in FEMM.

% Load vec2mat.m
pkg load communications;

a = load('B_femm.dat');

XX = vec2mat(a(:, 1), 129);
YY = vec2mat(a(:, 2), 129);

dx = XX(3, 1) - XX(2, 1);
dy = YY(1, 3) - YY(1, 2);

Bx = vec2mat(a(:, 3), 129)';
By = vec2mat(a(:, 4), 129)';

[phi, psi] = flowfun(Bx, By);

[ContourCFormat H] = contourc(psi, 20);

bufferC = [];

while ~isempty(ContourCFormat)
  N = ContourCFormat(2,1);
%  C{end+1} = ContourCFormat(:, 2 : 1+N); 
  bufferC = [bufferC ContourCFormat(:, 2 : 1+N)]; 
  ContourCFormat(:, 1 : 1+N) = []; % Remove processed points
end

bufferC(1, :) = bufferC(1, :)*dx;
bufferC(2, :) = bufferC(2, :)*dy;

bufferC = bufferC';

save -ascii 'B_lines.dat' bufferC

