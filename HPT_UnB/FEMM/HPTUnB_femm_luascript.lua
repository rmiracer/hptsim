-- Lua script to export data from FEMM to an ASCII file.
-- The ASCII file can be used as an input file for XOOPIC.

-- Units in mm
ni = 128
nj = 512
x1=000.0
x2=100.0
dx=(x2 - x1)/ni

y1=000.0
y2=1500.0
dy=(y2 - y1)/nj

handle=openfile("./B_femm.dat","w")

for j=0,nj-1,1 do
  for i=0,ni,1 do
  x=x1+i*dx
  y=y1+j*dy
  A,B1,B2=mo_getpointvalues(x,y)
  write(handle,y/1000," ",x/1000," "," ",B2*1e4," ",B1*1e4," 0.0\n")
 end
end

closefile(handle)
