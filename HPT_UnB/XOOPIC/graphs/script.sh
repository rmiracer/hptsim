if [ ! -d frames_phi ]; then
  mkdir frames_phi
fi

if [ ! -d frames_part ]; then
  mkdir frames_part
fi

if [ ! -d frames_Vz_z ]; then
  mkdir frames_Vz_z
fi

if [ -f temp.dmp ]; then
  rm temp.dmp
fi

#if [ -f nelec.dat ]; then
#  rm nelec.dat
#fi
#
#if [ -f nions.dat ]; then
#  rm nions.dat
#fi

BEGIN=1

END=`ls -1 ../*.dmp | wc -l`;


echo $BEGIN, $END

i=1
for file in `ls -1rt ../*.dmp`; do

  echo $file, $i/$END

  # Skip files already plotted
#  if [ $i -gt 110 ]; then

  ln -s $file ./temp.dmp

  h5dump -d /Fields/BdS       -o tempphi.dat -y temp.dmp > /dev/null
  h5dump -d /Fields/positions -o temppos.dat -y temp.dmp > /dev/null
  h5dump -g /electrons/ -o tempelectrons.dat     -y temp.dmp > /dev/null
  h5dump -g /ions1/      -o tempions1.dat         -y temp.dmp > /dev/null
  h5dump -g /ions2/      -o tempions2.dat         -y temp.dmp > /dev/null
  # Annoying XML output of h5dump
  h5dump -a /SpatialRegion/simulationTime -y temp.dmp | head -n 6 | tail -n 1 > time.dat

  paste temppos.dat tempphi.dat | awk '{print $1, $2, $5}' | sed 's/,/ /g' > phi.dat
  cat tempelectrons.dat         | awk '{print $1, $2}'     | sed 's/,/ /g' > electrons.dat
  cat tempions1.dat              | awk '{print $1, $2}'     | sed 's/,/ /g' > ions1.dat 
  cat tempions2.dat              | awk '{print $1, $2}'     | sed 's/,/ /g' > ions2.dat 

#  sed 's/,/ /g' phi_comma.dat > phi.dat

  gnuplot plot_phi.gplot
  mv fig.png frames_phi/frame`printf "%04d", $i`.png

  gnuplot plot_phi_particles.gplot
  mv fig.png frames_part/frame`printf "%04d", $i`.png

  gnuplot plot_Vz_z.gplot
  mv fig.png frames_Vz_z/frame`printf "%04d", $i`.png

  time=`cat time.dat`;
  wc -l electrons.dat | awk "{printf \"%f %f\n\", $time, \$1}"  >> nelec.dat
  wc -l ions1.dat     | awk "{printf \"%f %f\n\", $time, \$1}" >> nions1.dat
  wc -l ions2.dat     | awk "{printf \"%f %f\n\", $time, \$1}"  >> nions2.dat


  rm temp.dmp tempphi.dat temppos.dat tempelectrons.dat tempions1.dat tempions2.dat

#  fi

  i=$(($i+1))

done
